---
title: Manifesto - Reasons for a blog
author: Pietro Martinelli
date: 2012-02-01 18:39:34
tags:
---
I am a *serial reader*: I read tens of thousands of pages every year. I read novels, comics, essays about mathematics and sciences. But my job is Software Engineering, so I read plenty of books, articles, blog posts and wiki pages about this topic.
And I am a *serial polemicist*: against what I read, against my and other's mistakes in coding and design.
So, after much reading and arguing, I feel it's time to give something back: this blog is intendend to provide a place for my technical thoughts, reasoning, and for the way I feel about Software Engineering.
Some posts will be a kind of repetition of contents already published in the old, defunct blog of the Java User group of Brescia - *requiescat in pace*. Some posts will be a re-edition of contents, articles, code snippets, and so on, already published on my own wiki: I feel a blog is a more powerful and easy-to-follow kind of communication (you can ultimately simply register my blog's feed in your feed-reader of choice, and you'll be notified when I add a new post), so my intention is to migrate all content from the wiki to this new *virtual place*.
And there will be obviously new posts and new contents, too: code snippets written by me, technical debates about patterns and *anti-patterns*, *coding horrors* I run up against during my teaching at University (and not only...), thinking and reasoning at coffee dispenser, technical books reviews, links to posts and various resources in the web. Something about everything and everything about nothing: small - and hopefully tasty - contributions, just like peanuts.
And now: it's time to write! Stay tuned!

![JavaPeanuts logo](images/javapeanuts.jpg)