---
title: Embrace change
date: 2015-08-12 16:31:53
tags: job
---
So it is: three months ago I joined a new job position, switching from a big company to a little, agile one, after more than eight years of distinguished service :-).
Furthermore: I switched from a Java and JEE - centric technological environment to a more rich and various one - yet .NET and C# oriented.
So, my Java Peanuts will maybe become in the future C# Peanuts (or Node.js Peanuts, who knows...) or, more generally, Programming Peanuts: for the moment I'm still planning a little post series about my way from Java to .NET, so... if you are interested... stay tuned!