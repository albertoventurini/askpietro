---
title: 'Functions as first-class citizens: the shell-ish version'
date: 2020-12-12T18:00:00.000Z
author: 'Pietro Martinelli'
tags: code, bash, scripting, functional programming
categories:
    - functional programming
---
The idea to compose multiple functions together, passing one or more of them to another as parameters, generally referred to as  _using higher order functions_ is a pattern which I'm very comfortable with, since I read about ten years ago the very enlighting book  [Functional Thinking: Paradigm Over Syntax](https://www.blogger.com/blog/post/edit/6562129188834589284/5363854738070223135?hl=en#)  by Neal Ford. The main idea behind this book is that you can adopt a functional mindset programming in any language, wheter it supports  _function as first-class citizens_  or not. The examples in that book are mostly written in Java (version 5 o 6), a language that supports (something similar to)  _functions as first-class citizens_  only from version 8. As I said, it's more a matter of mindset than anything else.

So: a few days ago, during a lab of Operating System course, waiting for the solutions written by the students I was wondering If it is possible to take a  _functional_  approach composing functions (or something similar...) in a (bash) shell script.

_(More in detail: the problem triggering my thinking about this topic was "how to reuse a (not so much) complicated piece of code involving searching files and iterating over them in two different use cases, that differed only in the action applied to each file)_

My answer was _Probably yes!_, so I tried to write some code and ended up with the solution above.

**The main point**  is - imho - that as in a language supporting  _functions as first class citizens_  the bricks to be put together are  _functions_, in (bash) script the minimal bricks are  _commands_: generally speaking, a command can be a binary, or a  _script_  - but  _functions_  defined in (bash) scripts can be used as  _commands_, too. After making this mental switch, it's not particularly difficult to find a (simple) solution:

**`action0.sh`**  - An action to be applied to each element of a list
```bash
#!/bin/bash
echo "0 Processing $1"
```
**`action1.sh`** - This first action to be applied to each element of a list
```bash
#!/bin/bash
echo "1 Processing $1"
```
**`foreach.sh`**  - Something similar to  [`List<T>.ForEach(Action<T>)`](https://www.blogger.com/blog/post/edit/6562129188834589284/5363854738070223135?hl=en#)  extension method of .Net standard library(it's actually a  _high order program_)

```bash
#!/bin/bash
action=$1
shift
for x
do
    $action $x
done
```
**`main.sh`**  - The main program, reusing  _foreach_'s logic in more cases, passing to the  _high order program_  different  _actions_

```bash
#!/bin/bash
./foreach.sh ./action0.sh $(seq 1 6)
./foreach.sh ./action1.sh $(seq 1 6)

./foreach.sh ./action0.sh {A,B,C,D,E}19
./foreach.sh ./action1.sh {A,B,C,D,E}19
```
Following this approach, you can apply different actions to a bunch of files, without duplicating the code that finds them... and you do so applying a functional mindset to bash scripting!

In the same way it is possible to implement something like the classic  map _higher order function_  using functions in a bash script:

```bash
double () {
    expr $1 '*' 2
}

square () {
    expr $1 '*' $1
}

map () {
    f=$1
    shift
    for x
    do
        echo $($f $x)
    done
}

input=$(seq 1 6)
double_output=$(map "double" $input)
echo "double_output --> $double_output"
square_output=$(map "square" $input)
echo "square_output --> $square_output"
square_after_double_output=$(map "square" $(map "double" $input))
echo "square_after_double_output --> $square_after_double_output"
```
`square_after_double_output`, as expected, contains values  4, 16, 36, 64, 100, 144.

In conclusion... no matter what language you are using: using it functionally, composing bricks and higher order bricks together, it's just a matter of mindset!
