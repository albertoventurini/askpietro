---
title: Never executed - Never tested
author: Pietro Martinelli
categories:
  - coding horrors
date: 2012-02-28 18:59:04
tags: coding horrors, java
---
Code samples I'll publish in this post are not fakes: they come from real code, released into production.
And they are not only brilliant samples of never tested code: they are samples of **never executed code**!!! Indeed there are in these code snippets execution path which ever - ever! - fail. Read to believe...

**Sample #1** - `NullPointerException` at each catch execution
```java
MyClass result = null;
try {
    result = callMethod(...);
} catch(Exception e) {    // throws ever NullPointerException...
    result.registerException(e);
}
```

**Sample #2**: `ArrayIndexOutOfBoundException` at each catch execution

```java
try {
    result = callSomeMethod(...);
} catch(Exception e) {
    String[] messages = new String[3];
    messages[0] = ... ;
    messages[1] = ... ;
    messages[2] = ... ;
    // throws ever ArrayIndexOutOfBoundException ...
    messages[3] = ... ; 
    throw new CustomException(messages);
}
```

**Sample #3**: `ClassCastException` whenever `if`'s condition is verified

```java
public class AClass {
    ...
    public void aMethod(final Object obj) {
        ...
        if(!obj instanceof InterfaceXYZ) {
            final InterfaceXYZ xyz = (InterfaceXYZ)obj;
            ...
        }
        ...
    }
}
```